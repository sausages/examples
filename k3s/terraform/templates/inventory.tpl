[control]
%{ for controller in controllers ~}
${controller}
%{ endfor }

[worker]
%{ for worker in workers ~}
${ worker }
%{ endfor }

[k3s_cluster:children]
control
worker
