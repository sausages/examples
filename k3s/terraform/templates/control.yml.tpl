internal_address: "{{ ansible_eth0.ipv4.address }}"
public_address: ${ public_ip }
extra_agent_args: '--flannel-backend=none --no-flannel --disable-cloud-controller --node-taint CriticalAddonsOnly=true:NoExecute --no-deploy servicelb --kubelet-arg "cloud-provider=external" --server https://${ controller }:6443 --tls-san="${ public_ip }"'
