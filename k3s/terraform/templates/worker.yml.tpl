extra_agent_args: '--kubelet-arg="cloud-provider=external"'
ansible_ssh_common_args: '-o ProxyCommand="ssh -W %h:%p -q opensuse@${controller}"'
internal_address: "{{ ansible_eth0.ipv4.address }}"
