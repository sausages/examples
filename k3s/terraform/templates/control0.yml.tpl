internal_address: "{{ ansible_eth0.ipv4.address }}"
extra_agent_args: '--flannel-backend=none --no-flannel --disable-cloud-controller --node-taint CriticalAddonsOnly=true:NoExecute --no-deploy servicelb --kubelet-arg "cloud-provider=external" --cluster-init --tls-san="${ public_ip }"'
