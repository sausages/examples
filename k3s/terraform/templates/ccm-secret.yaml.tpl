apiVersion: v1
kind: Secret
metadata:
  name: cloud-config
  namespace: kube-system
type: Opaque
stringData:
  cloud.conf: |
    [Global]
    auth-url=https://compute.sausage.cloud:5000/v3
    username=${username}
    password=${password}
    region=Bunker
    tenant-id=${tenant}
    domain-id=${domain}
    
    [LoadBalancer]
    use-octavia=true
    subnet-id=${subnet}
    floating-network-id=5617d17e-fdc1-4aa1-a14b-b9b5136c65af

