variable "floating_ip_pool" {}
variable "floating_ip_pool_id" {}

variable "external_network_id" {
  description = "ID for external network"
  default     = "5617d17e-fdc1-4aa1-a14b-b9b5136c65af"
}

variable "network_cidr" {
  description = "CIDR range for subnet"
  default     = "192.168.20.0/24"
}

variable "cluster_name" {
  description = "A name for the cluster"
  default     = "sausagenetes"
}

variable "controllers_flavor_name" {
  description = "Flavor for the controller node"
  default     = "frankfurter"
}

variable "workers_flavor_name" {
  description = "Flavor for the worker nodes"
  default     = "frankfurter"
}

variable "num_controllers" {
  description = "Number of control nodes to provision"
  default     = "1"
}

variable "num_workers" {
  description = "Number of worker nodes"
  default     = "1"
}

variable "image_name" {
  description = "Name of OS image to use"
  default     = "openSUSE Leap 15.2"
}

variable "key_pair" {
  default = ""
}

variable "user_name" {
  default = "opensuse"
}

variable "controllers_tags" {
  description = "Role tags for control nodes"
  default     = ["controlplane", "etcd"]
}

variable "workers_tags" {
  description = "Role tags for control nodes"
  default     = ["worker"]
}

variable "openstack_username" {
  description = "Username for connecting to OpenStack's API"
  default     = ""
}

variable "openstack_password" {
  description = "Password for connecting to the OpenStack API"
  default     = ""
}
