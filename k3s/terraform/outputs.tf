output "controllers" {
  description = "control nodes public ip addresses"
  value       = module.controllers.public_ips
}

output "cluster-public-address" {
  description = "Public address for the cluster"
  value       = module.loadbalancer.public_ip
}
