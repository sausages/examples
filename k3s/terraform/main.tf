data "openstack_identity_auth_scope_v3" "scope" {
  name = "auth_scope"
}

resource "openstack_compute_secgroup_v2" "k3s" {
  name        = "k3s"
  description = "K3s default security group"

  rule {
    from_port   = 1
    to_port     = 65535
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 1
    to_port     = 65535
    ip_protocol = "udp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = -1
    to_port     = -1
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }
}

module "network" {
  source = "git::https://gitlab.com/sausages/terraform/tf-module-sausage-network.git"

  cluster_name        = var.cluster_name
  external_network_id = var.external_network_id
}

module "loadbalancer" {
  source = "git::https://gitlab.com/sausages/terraform/tf-module-sausage-lb.git"

  cluster_name      = var.cluster_name
  num_nodes         = length(var.num_controllers)
  network_id        = module.network.network_id
  subnet_id         = module.network.subnet_id
  floating_ip_pool  = var.floating_ip_pool
  node_ip_addresses = module.controllers.private_ips
  security_group_id = [openstack_compute_secgroup_v2.k3s.id]
}

module "controllers" {
  source = "git::https://gitlab.com/sausages/terraform/tf-module-sausage-node.git"

  num                         = var.num_controllers
  cluster_name                = var.cluster_name
  image_name                  = var.image_name
  flavor_name                 = var.controllers_flavor_name
  key_pair                    = var.key_pair
  user_name                   = var.user_name
  user_data                   = file("user-data.conf")
  security_groups             = ["k3s"]
  network_id                  = module.network.network_id
  associate_public_ip_address = true
  floating_ip_pool            = var.floating_ip_pool
  tags                        = var.controllers_tags
}

module "workers" {
  source = "git::https://gitlab.com/sausages/terraform/tf-module-sausage-node.git"

  num                         = var.num_workers
  cluster_name                = var.cluster_name
  image_name                  = var.image_name
  flavor_name                 = var.workers_flavor_name
  key_pair                    = var.key_pair
  user_name                   = var.user_name
  user_data                   = file("user-data.conf")
  security_groups             = ["k3s"]
  network_id                  = module.network.network_id
  associate_public_ip_address = false
  floating_ip_pool            = var.floating_ip_pool
  tags                        = var.workers_tags
}
