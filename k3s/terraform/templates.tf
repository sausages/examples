resource "local_file" "inventory" {
  filename = "${path.root}/../ansible/inventory/hosts"
  content = templatefile("${path.root}/templates/inventory.tpl", {
    controllers = module.controllers.public_ips,
    workers     = module.workers.private_ips
  })
}

resource "local_file" "control" {
  filename = "${path.root}/../ansible/inventory/group_vars/control.yml"
  content = templatefile("${path.root}/templates/control.yml.tpl", {
    controller = module.controllers.public_ips[0]
    public_ip  = module.loadbalancer.public_ip
  })
}

resource "local_file" "control0" {
  filename = "${path.root}/../ansible/inventory/host_vars/${module.controllers.public_ips[0]}"
  content = templatefile("${path.root}/templates/control0.yml.tpl", {
    controller = module.controllers.public_ips[0]
    public_ip  = module.loadbalancer.public_ip
  })
}

resource "local_file" "node" {
  filename = "${path.root}/../ansible/inventory/group_vars/worker.yml"
  content = templatefile("${path.root}/templates/worker.yml.tpl", {
    controller = module.controllers.public_ips[0]
  })
}

resource "local_file" "cloudconf" {
  filename = "${path.root}/../ansible/roles/addons/templates/ccm-secret.yaml"
  content = templatefile("${path.root}/templates/ccm-secret.yaml.tpl", {
    subnet = module.network.subnet_id
    tenant = data.openstack_identity_auth_scope_v3.scope.project_id
    domain = data.openstack_identity_auth_scope_v3.scope.project_domain_id
    username = var.openstack_username
    password = var.openstack_password
    floating-network-id = var.floating_ip_pool_id
  })
}
