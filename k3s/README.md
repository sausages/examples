# K3s on Sausage Cloud

This example will deploy a Kubernetes (1.20) cluster using
[K3s](https://k3s.io), and with [Cilium](https://cilium.io/) as the CNI.  The
default settings deploy a single "control" node and a single "worker" node.
These can both be adjusted;  Note that the former makes use of etcd and so an
odd number of controllers is required.

## Pre-reqs

Terraform 0.14 and Ansible 2.10

Edit `terraform/terraform.tfvars` and set the value for `ssh_key_pair` to be
the name of your SSH keypair.  You can also adjust the number of control and /
or worker here if necessary.

You also need to include your OpenStack API username and password.  This is
required for the Kubernetes to OpenStack integration via the [Cloud
Provider](https://github.com/kubernetes/cloud-provider-openstack).

## Deploy infrastructure

```sh
$ cd terraform
$ terraform init
$ terraform plan -out plan.out
$ terraform apply plan.out
```

```shell
Apply complete! Resources: 7 added, 0 changed, 0 destroyed.

Outputs:

cluster-public-address = "193.16.42.20"
controllers = [
  "193.16.42.63",
]
```

The `cluster-public-address` is the IP of the loadbalancer fronting the Kubernetes API server(s).

## Deploy K3s

Now we're ready to deploy K3s using Ansible:

```shell
cd ../ansible
$ ansible-playbook -i inventory/hosts site.yml
```

## Configure `kubectl`

With our K3s Kubernetes cluster deployed, we need to first grab the generated `kubeconfig` from our controller:

```shell
$ cd ..
$ scp opensuse@193.16.42.20:.kube/config ./kubeconfig
$ export KUBECONFIG=$(pwd)/kubeconfig
$ kubectl get nodes
NAME          STATUS   ROLES    AGE   VERSION
controller0   Ready    master   62s   v1.18.4+k3s1
worker0       Ready    <none>   37s   v1.18.4+k3s1
```

If everything's working correctly, you should also be able to see that an
external IP address has been successfully assigned to the `traefik`
LoadBalancer:

```shell
$ kubectl get svc/traefik -n kube-system
NAME      TYPE           CLUSTER-IP      EXTERNAL-IP    PORT(S)                      AGE
traefik   LoadBalancer   10.43.110.188   193.16.42.47   80:31959/TCP,443:31005/TCP   4m7s
```

All done!

## Notes / Gotchas

### `terraform destroy` hanging 

The OpenStack CCM will create Octavia loadbalancers for any Kubernetes Service
of type 'LoadBalancer'.  This includes at least one by default for Traefik.  As
these are created outside of Terraform, it'll hang when you try to destroy the
environment unless you delete these LBs first.
