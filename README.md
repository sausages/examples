# Getting to grips with your sausage
## Working examples

This repo contains guides for various working examples of how to deploy virtual infrastructure of various shapes and sizes on Sausage Cloud.

Right now the examples are mostly concerned with setting up a Kubernetes cluster of some shape and size.  You've currently two options for bootstrapping Kubernetes:

* Use Rancher's [RKE](https://rancher.com/docs/rke/latest/en/) via Terraform.  This option gives you a production-ready cluster using industry-standard tooling.
* Use Rancher's [K3s](https://k3s.io).  K3s is a lightweight Kubernetes distribution and swaps out etcd for sqlite.  The result is that it's a lot less resource intensive, but at the expense of resilience of the cluster control plane.

For deployment on Sausage Cloud, I'd recommend K3s.

## Pre-requisites

Most guides in this repo have the same set of pre-requisites - it's recommended that you install these essential tools before interacting with the platform.

### OpenStack CLI

OpenStack provides a client CLI written in Python, which can be used interactively and as a library as a way of working with the platform's APIs.  You can find the official documentation for getting the CLI installed [here](https://pypi.org/project/python-openstackclient/).  However, if you have Docker available then the easiest way might be to use a Docker container.

Create a shell script somewhere in your `$PATH` called `openstack`, with the following contents:

```shell
#!/usr/bin/env bash
docker run -it --rm --name openstack \
       -e TERM -e OS_CLOUD -u $(id -u):$(id -g) \
       -v $HOME/.config/openstack:/home/user/.config/openstack \
       yankcrime/openstackcli:20203003 "$@"
```

Then we need to create a `clouds.yaml` file with the details necessary to authenticate against Sausage Cloud.  Create a folder `~/.config/openstack` and a file called `~/.config/openstack/clouds.yaml` with this in it:

```yaml
clouds:
  sausage:
    identity_api_version: 3
    interface: public
    region_name: Bunker
    auth:
      domain_name: 'Default'
      project_domain_name: 'Default'
      project_name: 'your_projectname'
      username: 'your_username'
      password: 'your_password'
      auth_url: 'https://compute.sausage.cloud:5000/v3'
```

Replace `your_projectname`, `your_username`, and `your_password` with the relevant details.

Now set an environment variable, `OS_CLOUD` with the value of `sausage` and we're good to start running commands via the OpenStack CLI:

```shell
$ export OS_CLOUD=sausage
$ ~/bin/openstack token issue -c user_id
+---------+----------------------------------+
| Field   | Value                            |
+---------+----------------------------------+
| user_id | 12345678901234567789011921972872 |
+---------+----------------------------------+

$ ~/bin/openstack server list -c Name
+---------+
| Name    |
+---------+
| zarquon |
| garkbit |
+---------+
```

_NB_: The first time you run this script your machine will need to download the referenced Docker image, so this might take a little while before any results are returned.  Each subsequent run should be nearly instantaneous.


### Terraform

The other tool we recommend you install in order to make use of the examples in this repo is [Terraform](https://www.terraform.io/).  You can download it from [here](https://www.terraform.io/downloads.html).
